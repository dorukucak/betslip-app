import React, { Dispatch, SetStateAction } from 'react'
import { IbetSummary } from '../../../../layout/Betslip'

import './style.css'

interface IStakeInput {
  onChange: (input: any) => void
}

const StakeInput = ({ onChange }: IStakeInput) => {
  return (
    <div className="stake">
      <p>£</p>
      <input
        className="stake-input"
        type="number"
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  )
}

export default StakeInput
