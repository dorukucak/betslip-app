import React from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import { BetSlip } from './components'

function App() {
  return (
    <div className="App">
      <BetSlip />
    </div>
  )
}

export default App
