import React from 'react'
import StakeInput from '../stake-input'

import './style.css'

interface Iodd {
  betName: string
  odds: number
  handleInput: (input: number) => void
}

const SlipCard = ({ betName, odds, handleInput }: Iodd) => {
  return (
    <div className="slip-card">
      <div className="slip-info">
        <span>Player: {betName}</span>
        <span>Best odds: {odds}</span>
      </div>
      <div>
        <StakeInput onChange={handleInput} />
      </div>
    </div>
  )
}

export default SlipCard
