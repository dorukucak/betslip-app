import React from 'react'

import './style.css'

export interface ISuccess {
  totalStake: number
}

const BetSuccess = ({ totalStake }: ISuccess) => {
  return (
    <div className="bet-success">
      <div className="receipt-title">Receipt</div>
      <div className="receipt-text">Your bet has been placed!</div>
      <div className="receipt-text">Total Stake:</div>
      <div className="receipt-total">£ {totalStake}</div>
    </div>
  )
}

export { BetSuccess }
