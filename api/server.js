const express = require('express')
const bodyParser = require('body-parser')
const { body, validationResult } = require('express-validator')
const app = express()
const cors = require('cors')

const data = require('./data/data.json')

app.use(cors())
app.use(bodyParser.json())

const getMaxOdds = (arr) => Math.max(...arr.map((o) => o.oddsDecimal), 0)
const odds = data?.bets?.map((bet) => ({
  name: bet.name,
  odds: getMaxOdds(bet.odds),
}))

app.get('/decimalOddsMoreThanTwo', (req, res) => {
  if (!data) {
    res.status(500).json({ status: 500, message: 'Data not available' })
    return
  }
  const moreThanTwo = odds?.filter((odd) => odd.odds >= 2)
  res.status(200).json(moreThanTwo)
})

app.get('/decimalOddsLessThanTwo', (req, res) => {
  if (!data) {
    res.status(500).json({ status: 500, message: 'Data not available' })
    return
  }
  const lessThanTwo = odds?.filter((odd) => odd.odds < 2)
  res.status(200).json(lessThanTwo)
})

app.post('/submitBet', (req, res) => {
  if (!req.body?.[0]?.bets) {
    return res.status(400).json({ status: 'Payload error' })
  }

  const totalStake = Object.values(req.body[0].bets)
    .reduce((acc, bet) => bet.total + acc, 0)
    .toFixed(2)

  if (totalStake < 1) {
    return res.status(400).json({ status: 'Stake cannot be less than 1' })
  }

  setTimeout(() => {
    res.status(200).json({ status: 'Success', totalStake: totalStake })
  }, 2000)
})

app.listen(4000, () => {
  console.log('Example app listening on port 4000!')
})
