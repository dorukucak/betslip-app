import React, { useEffect, useState } from 'react'
import { BetSlipForm } from '../ui/betslip'
import { BetSuccess, ISuccess } from '../ui/betslip/result/success'
import fetcher from '../../libs/fetcher'
import { endpoints } from '../../api'

import './betslip.css'

export interface IbetSummary {
  bets: [{ name: string; bestOdds: number }]
  totalStake: number
}

const BetSlip = () => {
  const [slipState, setSlipState] = useState<string>('form')
  const [bets, setBets] = useState<Array<any>>([])
  const [totalStake, setTotalStake] = useState<number>(0)
  const [apiLoading, setApiLoading] = useState<boolean>(false)

  const fetchoddsLessThanTwo = async () => {
    setApiLoading(true)
    const filteredOdds = await fetcher({
      url: endpoints.oddsLessThanTwo,
      method: 'GET',
    })
    setBets(filteredOdds)
    setApiLoading(false)
  }

  const fetchoddsMoreThanTwo = async () => {
    setApiLoading(true)
    const filteredOdds = await fetcher({
      url: endpoints.oddsMoreThanTwo,
      method: 'GET',
    })
    setBets(filteredOdds)
    setApiLoading(false)
  }

  const submitBet = async (betSummary: IbetSummary) => {
    setApiLoading(true)
    const response = await fetcher({
      url: endpoints.submitBet,
      method: 'POST',
      data: betSummary,
    })

    if (response?.response?.status === 400) {
      setApiLoading(false)
      return
    }

    setSlipState('success')
    setTotalStake(response?.totalStake)
    setApiLoading(false)
  }

  useEffect(() => {
    fetchoddsLessThanTwo()
  }, [])

  return (
    <div className="betslip-layout">
      {slipState === 'form' && (
        <BetSlipForm
          setSlipState={(val) => setSlipState(val)}
          bets={bets}
          fetchMore={fetchoddsLessThanTwo}
          fetchLess={fetchoddsMoreThanTwo}
          apiLoading={apiLoading}
          onSubmit={submitBet}
        />
      )}
      {slipState === 'success' && <BetSuccess totalStake={totalStake} />}
      {/* {slipState === 'error' &&  <BetError />} */}
    </div>
  )
}

export { BetSlip }
