import React from 'react'
import './loading.css'

type Props = {}

const Loading = (props: Props) => {
  return <div className="loader" />
}

export default Loading
