const endpoints = {
  oddsLessThanTwo: 'decimalOddsLessThanTwo',
  oddsMoreThanTwo: 'decimalOddsMoreThanTwo',
  submitBet: 'submitBet',
}

export { endpoints }
