import React from 'react'

import './style.css'

export interface IDropdown {
  fetchMore: () => void
  fetchLess: () => void
}

const Dropdown = (props: IDropdown) => {
  const { fetchMore, fetchLess } = props

  const handleSelect = async (val: string) => {
    switch (val) {
      case 'more':
        fetchMore()
        break
      case 'less':
        fetchLess()
        break
      default:
        break
    }
  }

  return (
    <select
      className="dropdown"
      onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
        handleSelect(e.target.value)
      }
    >
      <option value={'more'}>Odds Less Than 2</option>
      <option value={'less'}>Odds More Than 2</option>
    </select>
  )
}

export { Dropdown }
