import axios, { AxiosError } from 'axios'

interface Ifetcher {
  url: string
  method: string
  data?: { totalStake: number }
}

const fetcher = async (fn: Ifetcher) => {
  try {
    const response = await axios({
      method: fn.method,
      url: `http://localhost:4000/${fn.url}`,
      data: fn.data,
      headers: {
        'Content-Type': 'application/json',
      },
    })
    return response?.data
  } catch (error: any) {
    alert(error.response.data.status)
    return error
  }
}

export default fetcher
