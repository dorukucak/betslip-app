import React from 'react'
import { IbetSummary } from '../../layout/Betslip'
import Loading from '../loader'
import './button.css'

interface Button {
  onClick: (input: any) => void
  loading?: boolean
  text: string
}

const Button = ({ onClick, loading, text }: Button) => {
  return (
    <button className="btn-submit" disabled={loading} onClick={onClick}>
      {loading && <Loading />}
      <span>{text}</span>
    </button>
  )
}

export default Button
