import React, { Dispatch, SetStateAction, useState } from 'react'
import Button from '../../button'
import { Dropdown, IDropdown } from './dropdown'
import SlipCard from './slip-card'

import './style.css'

interface IForm {
  setSlipState: Dispatch<SetStateAction<string>>
  bets: Array<{ name: string; odds: number }>
  fetchMore: IDropdown['fetchMore']
  fetchLess: IDropdown['fetchLess']
  onSubmit: (betSummary: any) => void
  apiLoading: boolean
}

const BetSlipForm = ({
  bets,
  fetchMore,
  fetchLess,
  onSubmit,
  apiLoading,
}: IForm) => {
  const [formData, setFormData] = useState<Array<any>>([{ bets: {} }])

  const handleInput = (input: number, name: string, odds: number) => {
    let data = [...formData]
    let total = odds * input
    data[0].bets[name] = { odds, input, total }
    setFormData(data)
  }

  return (
    <div className="betslip-form">
      <div className="bet-title">
        <p>Betslip</p>
        <Dropdown fetchMore={fetchMore} fetchLess={fetchLess} />
      </div>

      <div className="slip-cards">
        {bets?.map((odd, index) => (
          <SlipCard
            key={odd.name}
            betName={odd.name}
            odds={odd.odds}
            handleInput={(input: number) =>
              handleInput(input, odd.name, odd.odds)
            }
          />
        ))}
      </div>
      <div className="betslip-button">
        <Button
          text="Bet Now"
          onClick={() => onSubmit(formData)}
          loading={apiLoading}
        />
      </div>
    </div>
  )
}

export { BetSlipForm }
